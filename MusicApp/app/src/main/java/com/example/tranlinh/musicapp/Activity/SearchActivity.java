package com.example.tranlinh.musicapp.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tranlinh.musicapp.Activity.TabsFragment.TabAlbums;
import com.example.tranlinh.musicapp.Activity.TabsFragment.TabArtists;
import com.example.tranlinh.musicapp.Activity.TabsFragment.TabSongs;
import com.example.tranlinh.musicapp.Object.Albums;
import com.example.tranlinh.musicapp.Object.Artists;
import com.example.tranlinh.musicapp.Object.Songs;
import com.example.tranlinh.musicapp.Object.StaticVariable;
import com.example.tranlinh.musicapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TranLinh on 12/16/2016.
 */

public class SearchActivity extends AppCompatActivity {
    Toolbar toolbar;
    private EditText editKeyWord;
    private List<Songs> suggestSongList = new ArrayList<Songs>();
    private List<Albums> suggestAlbumList = new ArrayList<Albums>();
    private List<Artists> suggestArtistList = new ArrayList<Artists>();
    private RecyclerView songView,albumView,artistView;
    private TextView songNoti,albumNoti,artistNoti;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        findViewById();
        handleEvent();
    }
    protected void findViewById(){
        toolbar = (Toolbar)findViewById(R.id.search_ac_toolbar);
        editKeyWord = (EditText)findViewById(R.id.search_ac_keyWord);
        songView = (RecyclerView)findViewById(R.id.search_ac_songList);
        albumView = (RecyclerView)findViewById(R.id.search_ac_albumList);
        artistView = (RecyclerView)findViewById(R.id.search_ac_artistList);
        songNoti = (TextView)findViewById(R.id.search_ac_songNoti);
        albumNoti = (TextView)findViewById(R.id.search_ac_albumNoti);
        artistNoti = (TextView)findViewById(R.id.search_ac_artistNoti);
    }
    protected void handleEvent(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        songView.setHasFixedSize(true);
        songView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
        albumView.setHasFixedSize(true);
        albumView.setLayoutManager(new GridLayoutManager(SearchActivity.this,4));
        artistView.setHasFixedSize(true);
        artistView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));

        editKeyWord.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                suggestSongList = new ArrayList<Songs>();
                suggestArtistList = new ArrayList<Artists>();
                suggestAlbumList = new ArrayList<Albums>();
                if(charSequence.toString().isEmpty()){
                    return;
                }
                charSequence = charSequence.toString().toLowerCase();
                for(int i = 0; i < StaticVariable.songsList.size(); i ++){
                    if(StaticVariable.songsList.get(i).getName().toLowerCase().contains(charSequence)){
                        suggestSongList.add(StaticVariable.songsList.get(i));
                    }
                }
                for(int i = 0; i < StaticVariable.albumsList.size(); i ++){
                    if(StaticVariable.albumsList.get(i).getAlbumName().toLowerCase().contains(charSequence)){
                        suggestAlbumList.add(StaticVariable.albumsList.get(i));
                    }
                }
                for(int i = 0; i < StaticVariable.artistsList.size(); i ++){
                    if(StaticVariable.artistsList.get(i).getArtistName().toLowerCase().contains(charSequence)){
                        suggestArtistList.add(StaticVariable.artistsList.get(i));
                    }
                }
                if(!suggestSongList.isEmpty()){
                    songNoti.setVisibility(View.VISIBLE);
                } else{
                    songNoti.setVisibility(View.INVISIBLE);
                }
                if(!suggestAlbumList.isEmpty()){
                    albumNoti.setVisibility(View.VISIBLE);
                } else{
                    albumNoti.setVisibility(View.INVISIBLE);
                }
                if(!suggestArtistList.isEmpty()){
                    artistNoti.setVisibility(View.VISIBLE);
                } else{
                    artistNoti.setVisibility(View.INVISIBLE);
                }
                TabSongs.ContentAdapter songAdapter = new TabSongs.ContentAdapter(suggestSongList);
                songView.setAdapter(songAdapter);
                TabAlbums.ContentAdapter albumAdapter = new TabAlbums.ContentAdapter(suggestAlbumList);
                albumView.setAdapter(albumAdapter);
                TabArtists.ContentAdapter artistAdapter = new TabArtists.ContentAdapter(suggestArtistList);
                artistView.setAdapter(artistAdapter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}
