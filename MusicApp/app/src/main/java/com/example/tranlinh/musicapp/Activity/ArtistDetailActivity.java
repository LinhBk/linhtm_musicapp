package com.example.tranlinh.musicapp.Activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.tranlinh.musicapp.Activity.TabsFragment.TabAlbums;
import com.example.tranlinh.musicapp.Activity.TabsFragment.TabSongs;
import com.example.tranlinh.musicapp.Object.Albums;
import com.example.tranlinh.musicapp.Object.Artists;
import com.example.tranlinh.musicapp.Object.Songs;
import com.example.tranlinh.musicapp.Object.StaticVariable;
import com.example.tranlinh.musicapp.R;
import com.example.tranlinh.musicapp.Support.GetSongInfoFromStorage.GetSongInfoFromStorage;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by TranLinh on 12/15/2016.
 */

public class ArtistDetailActivity extends AppCompatActivity {
    private RecyclerView albumList,songList;
    private Toolbar toolbar;
    private int position;
    private Artists artist;
    private TextView totalAlbums, totalSongs;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.artist_detail);
        getData();
        findViewById();
        handleEvent();
        setupRecycleView();
    }
    protected void getData(){
        position = getIntent().getIntExtra(StaticVariable.ARTIST_KEY,0);
        artist = StaticVariable.artistsList.get(position);
    }
    protected void findViewById(){
        toolbar = (Toolbar)findViewById(R.id.artist_detail_ac_toolbar);
        totalAlbums = (TextView)findViewById(R.id.artist_detail_ac_totalAlbums);
        totalSongs = (TextView)findViewById(R.id.artist_detail_ac_totalSongs);

    }
    protected void handleEvent(){
        toolbar.setTitle(artist.getArtistName());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        totalAlbums.setText("Total "+artist.getTotalAlbum()+" Albums: ");
        totalSongs.setText("Total "+artist.getTotalTrack()+" Songs: ");
    }
    protected void setupRecycleView(){
        albumList= (RecyclerView)findViewById(R.id.artist_detail_ac_albumlist);
        List<Albums> albums = GetSongInfoFromStorage.getAlbumInfo(ArtistDetailActivity.this,artist.getArtistName());
        TabAlbums.ContentAdapter adapter = new TabAlbums.ContentAdapter(albums);
        albumList.setAdapter(adapter);
        albumList.setHasFixedSize(true);
        albumList.setLayoutManager(new GridLayoutManager(ArtistDetailActivity.this,3));

        songList= (RecyclerView)findViewById(R.id.artist_detail_ac_songlist);
        List<Songs> songs = GetSongInfoFromStorage.getSongImage(ArtistDetailActivity.this,StaticVariable.albumsList,artist.getArtistName());
        TabSongs.ContentAdapter adapter1 = new TabSongs.ContentAdapter(songs);
        songList.setAdapter(adapter1);
        songList.setHasFixedSize(true);
        songList.setLayoutManager(new LinearLayoutManager(ArtistDetailActivity.this));
    }
}
