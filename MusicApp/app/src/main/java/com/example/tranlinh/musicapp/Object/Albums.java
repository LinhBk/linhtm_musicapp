package com.example.tranlinh.musicapp.Object;

/**
 * Created by TranLinh on 12/16/2016.
 */

public class Albums {
    private long albumId;
    private String albumName;
    private String artistName;
    private int totalSongs;
    private String image;


    public Albums(long albumId,String albumName,String artistName,int totalSongs,String image){
        this.albumId = albumId;
        this.albumName = albumName;
        this.artistName = artistName;
        this.totalSongs = totalSongs;
        this.image = image;
    }

    public int getTotalSongs() {
        return totalSongs;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getImage() {
        return image;
    }
}
