package com.example.tranlinh.musicapp.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.tranlinh.musicapp.Activity.TabsFragment.TabSongs;
import com.example.tranlinh.musicapp.Object.Songs;
import com.example.tranlinh.musicapp.Object.StaticVariable;
import com.example.tranlinh.musicapp.R;

/**
 * Created by TranLinh on 12/15/2016.
 */

public class PlaylistDetailActivity extends AppCompatActivity {
    public RecyclerView recyclerView;
    public FloatingActionButton fab;
    public ImageButton prevBtn,shuffleBtn,repeatBtn,nextbtn;
    private Toolbar toolbar;
    private ImageView image;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playlist_detail);
        findViewById();
        handleEvent();
        setupRecycleView();
    }
    protected void setupRecycleView(){

        recyclerView= (RecyclerView)findViewById(R.id.playlist_detail_ac_recycleview);
        TabSongs.ContentAdapter adapter = new TabSongs.ContentAdapter(StaticVariable.playList);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(PlaylistDetailActivity.this));
    }

    protected void findViewById(){
        toolbar = (Toolbar)findViewById(R.id.playlist_detail_ac_toolbar);
        prevBtn = (ImageButton)findViewById(R.id.playlist_detail_ac_prevBtn);
        shuffleBtn = (ImageButton)findViewById(R.id.playlist_detail_ac_shuffleBtn);
        repeatBtn = (ImageButton)findViewById(R.id.playlist_detail_ac_repeatBtn);
        nextbtn = (ImageButton)findViewById(R.id.playlist_detail_ac_nextBtn);
        fab = (FloatingActionButton) findViewById(R.id.playlist_detail_ac_playPauseButton);
        image = (ImageView)findViewById(R.id.playlist_detail_ac_songImage);
    }
    protected void setData(){
        StaticVariable.currentSong = StaticVariable.songsList.get(StaticVariable.currentPosition);
        image.setImageDrawable(Drawable.createFromPath(StaticVariable.currentSong.getImage()));
        toolbar.setTitle(StaticVariable.currentSong.getName());
    }
    protected void handleEvent(){
        setData();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSnackBar(view,"Next");
                ViewCompat.setElevation(nextbtn,6);
                if(StaticVariable.currentPosition == StaticVariable.songsList.size()-1){
                    StaticVariable.currentPosition = 0;
                    setData();
                } else{
                    StaticVariable.currentPosition ++;
                    setData();
                }
            }
        });
        shuffleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(StaticVariable.isShuffling == 0){
                    StaticVariable.isShuffling = 1;
                    showSnackBar(view,"Shuffle Mode: On");
                    shuffleBtn.setColorFilter(R.color.icon_on_click);
                } else{
                    showSnackBar(view,"Shuffle Mode: Off");
                    StaticVariable.isShuffling = 0;
                    shuffleBtn.setColorFilter(Color.WHITE);
                }

            }
        });
        repeatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(StaticVariable.isRepeating == 0){
                    StaticVariable.isRepeating = 1;
                    showSnackBar(view,"Repeat Mode: On");
                    repeatBtn.setColorFilter(R.color.icon_on_click);
                } else{
                    showSnackBar(view,"Repeat Mode: Off");
                    StaticVariable.isRepeating = 0;
                    repeatBtn.setColorFilter(Color.WHITE);
                }
            }
        });
        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSnackBar(view,"Previous");
                if (StaticVariable.currentPosition == 0){
                    StaticVariable.currentPosition = StaticVariable.songsList.size() -1;
                    setData();
                } else{
                    StaticVariable.currentPosition --;
                    setData();
                }
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(StaticVariable.isPlaying == 0){
                    StaticVariable.isPlaying = 1;
                    fab.setImageResource(R.drawable.ic_pause);
                    showSnackBar(v,"Playing");
                } else{
                    StaticVariable.isPlaying = 0;
                    fab.setImageResource(R.drawable.ic_play);
                    showSnackBar(v,"Pause");
                }
            }
        });
    }
    private void showSnackBar(View v,String messagge){
        Snackbar noti = Snackbar.make(v,messagge,Snackbar.LENGTH_LONG);
        View sbView = noti.getView();
        sbView.setBackgroundColor(Color.parseColor("#7A1EA1"));
        noti.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_playlist_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if(id == R.id.action_sort){
            Intent intent = new Intent(PlaylistDetailActivity.this,SortActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
