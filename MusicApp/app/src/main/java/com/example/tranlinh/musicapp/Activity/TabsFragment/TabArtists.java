package com.example.tranlinh.musicapp.Activity.TabsFragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tranlinh.musicapp.Activity.ArtistDetailActivity;
import com.example.tranlinh.musicapp.Object.Artists;
import com.example.tranlinh.musicapp.Object.StaticVariable;
import com.example.tranlinh.musicapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TranLinh on 12/14/2016.
 */

public class TabArtists extends android.support.v4.app.Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView)inflater.inflate(R.layout.recycle_view,container,false);
        ContentAdapter adapter = new ContentAdapter(StaticVariable.artistsList);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));

        return recyclerView;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView artistImage;
        public TextView artistName,artistTotalAlbums,artistTotalSongs;

        public ViewHolder(View itemView) {
            super(itemView);
        }
        public ViewHolder(LayoutInflater inflater,ViewGroup parent){
            super(inflater.inflate(R.layout.artist_items,parent,false));
            artistImage = (ImageView)itemView.findViewById(R.id.artist_items_artistImages);
            artistName = (TextView)itemView.findViewById(R.id.artist_items_artistName);
            artistTotalAlbums = (TextView)itemView.findViewById(R.id.artist_items_artistTotalAlbum);
            artistTotalSongs = (TextView)itemView.findViewById(R.id.artist_items_artistTotalSong);
        }
    }
    public static class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {
        // Set numbers of List in RecyclerView.

        private  List<Artists> artistsList = new ArrayList<Artists>();
        public ContentAdapter(List<Artists> artistsList){
            this.artistsList = artistsList;
        }
        public ContentAdapter() {
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            // no-op
            holder.artistName.setText(artistsList.get(position).getArtistName());
            holder.artistTotalAlbums.setText("Total Albums: \t"+ String.valueOf(artistsList.get(position).getTotalAlbum()));
            holder.artistTotalSongs.setText("Total Songs: \t" + String.valueOf(artistsList.get(position).getTotalTrack()));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), ArtistDetailActivity.class);
                    intent.putExtra(StaticVariable.ARTIST_KEY,position);
                    view.getContext().startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return artistsList.size();
        }
    }
}
