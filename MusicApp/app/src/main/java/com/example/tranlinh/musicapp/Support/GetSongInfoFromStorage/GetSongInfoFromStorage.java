package com.example.tranlinh.musicapp.Support.GetSongInfoFromStorage;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.example.tranlinh.musicapp.Object.Albums;
import com.example.tranlinh.musicapp.Object.Artists;
import com.example.tranlinh.musicapp.Object.Songs;
import com.example.tranlinh.musicapp.Object.StaticVariable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TranLinh on 12/16/2016.
 */

public class GetSongInfoFromStorage {
    private static Cursor getCursor(Context context, int type){
        Cursor cursor;
        Uri uri = null;
        String selection="";
        String[] STAR = { "*" };
        switch (type){
            case 1:{
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                selection= MediaStore.Audio.Media.IS_MUSIC + " != 0";
                break;
            }
            case 2:{
                uri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;
                selection = MediaStore.Audio.Artists.ARTIST +" !=0";
                break;
            }
            case 3:{
                uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
                selection = MediaStore.Audio.Albums.ALBUM +" !=0";
                break;
            }
        }

        cursor = context.getContentResolver().query(uri, STAR, selection, null, null);
        return cursor;
    }
    public static List<Songs> getSongInfo(Context context){
        List<Songs> list = new ArrayList<Songs>();
        Cursor cursor = getCursor(context,1);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String songName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                    String albumName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                    String artistName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
                    Songs track = new Songs(songName,artistName,albumName,path,duration);
                    list.add(track);
                } while (cursor.moveToNext());
            }
        }

        return list;
    }
    public static List<Songs> getSongInfo(Context context,String input){
        List<Songs> list = new ArrayList<Songs>();
        Cursor cursor = getCursor(context,1);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String artistName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    String songName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                    String albumName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                    if(artistName.equals(input) == false && albumName.equals(input) == false){
                        continue;
                    }
                    String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
                    Songs songs = new Songs(songName,artistName,albumName,path,duration);
                    list.add(songs);
                } while (cursor.moveToNext());
            }
        }
        return list;
    }
    public static List<Songs> getSongImage(List<Albums> albumList, List<Songs> trackList){
        int i,j;
        for(i = 0; i<trackList.size();i++){
            for(j =0 ; j<albumList.size();j++){
                if(trackList.get(i).getAlbumName().equals(albumList.get(j).getAlbumName())){
                    trackList.get(i).setImage(albumList.get(j).getImage());
                    break;
                }
            }
        }
        return trackList;
    }
    public static List<Songs> getSongImage(Context context,List<Albums> albumsList){
        List<Songs> songsList = GetSongInfoFromStorage.getSongInfo(context);
        songsList = GetSongInfoFromStorage.getSongImage(albumsList,songsList);
        return songsList;
    }
    public static List<Songs> getSongImage(Context context,List<Albums> albumsList,String keyword){
        List<Songs> songsList = GetSongInfoFromStorage.getSongInfo(context,keyword);
        songsList = GetSongInfoFromStorage.getSongImage(albumsList,songsList);
        return songsList;
    }
    public static List<Songs> getSongImage(String albumName,List<Songs> trackList){
        int i;
        Albums album = null;
        for(i = 0; i < StaticVariable.albumsList.size(); i++){
            if(StaticVariable.albumsList.get(i).getAlbumName().equals(albumName)){
                album  = StaticVariable.albumsList.get(i);
                break;
            }
        }
        for(i = 0; i < trackList.size(); i++){
            trackList.get(i).setImage(album.getImage());
        }
        return trackList;
    }
    public static List<Artists> getArtistInfo(Context context){
        List<Artists> list = new ArrayList<Artists>();
        Cursor cursor = getCursor(context,2);
        if(cursor!=null){
            if(cursor.moveToFirst()){
                do{
                    String artistName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Artists.ARTIST));
                    String totalAlbums = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_ALBUMS));
                    String totalTracks = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_TRACKS));
                    Artists artist = new Artists(artistName,Integer.parseInt(totalTracks), Integer.parseInt(totalAlbums));
                    list.add(artist);
                } while (cursor.moveToNext());
            }
        }
        return list;
    }
    public static List<Albums> getAlbumInfo(Context context){
        List<Albums> list = new ArrayList<Albums>();
        Cursor cursor = getCursor(context,3);
        if(cursor!=null){
            if(cursor.moveToFirst()){
                do{
                    Long albumId = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums._ID));
                    String albumName = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM));
                    String artist = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ARTIST));
                    String totalTracks = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.NUMBER_OF_SONGS));
                    String image = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM_ART));
                    Albums album = new Albums(albumId,albumName,artist,Integer.parseInt(totalTracks),image);
                    list.add(album);
                } while(cursor.moveToNext());
            }
        }
        return list;
    }
    public static List<Albums> getAlbumInfo(Context context,String mArtist){
        List<Albums> list = new ArrayList<Albums>();
        Cursor cursor = getCursor(context,3);
        if(cursor!=null){
            if(cursor.moveToFirst()){
                do{
                    Long albumId = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums._ID));
                    String albumName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM));
                    String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST));
                    if(artist.equals(mArtist) == false){
                        continue;
                    }
                    String totalTracks = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS));
                    String image = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM_ART));

                    Albums album = new Albums(albumId,albumName,artist,Integer.parseInt(totalTracks),image);

                    list.add(album);
                } while(cursor.moveToNext());
            }
        }
        return list;
    }
}
