package com.example.tranlinh.musicapp.Object;

/**
 * Created by TranLinh on 12/16/2016.
 */

public class Artists {
    private String artistName;
    private int totalTrack=0;
    private int totalAlbum=0;
    public Artists(String name, int totalTrack, int totalAlbum){
        this.artistName = name;
        this.totalAlbum = totalAlbum;
        this.totalTrack = totalTrack;
    }

    public int getTotalTrack() {
        return totalTrack;
    }

    public int getTotalAlbum() {
        return totalAlbum;
    }

    public String getArtistName() {
        return artistName;
    }
}
