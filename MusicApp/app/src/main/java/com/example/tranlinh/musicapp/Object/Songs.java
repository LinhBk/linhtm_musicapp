package com.example.tranlinh.musicapp.Object;

/**
 * Created by TranLinh on 12/16/2016.
 */

public class Songs {
    private String name;
    private String artistName;
    private String albumName;
    private String path;
    private String image;
    private String duration;
    public Songs(String name,String artistName,String albumName,String path,String duration){
        this.name = name;
        this.artistName = artistName;
        this.albumName = albumName;
        this.path = path;
        this.duration = duration;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPath() {
        return path;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getName() {
        return name;
    }

    public String getDuration() {
        return duration;
    }
}
