package com.example.tranlinh.musicapp.Activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tranlinh.musicapp.Activity.TabsFragment.TabSongs;
import com.example.tranlinh.musicapp.Object.Albums;
import com.example.tranlinh.musicapp.Object.Songs;
import com.example.tranlinh.musicapp.Object.StaticVariable;
import com.example.tranlinh.musicapp.R;
import com.example.tranlinh.musicapp.Support.GetSongInfoFromStorage.GetSongInfoFromStorage;

import java.util.List;

/**
 * Created by TranLinh on 12/15/2016.
 */

public class AlbumDetailActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private Toolbar toolbar;
    private int position;
    private Albums album;
    private TextView totalSongs;
    private ImageView topToolbarImage;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_detail);
        getData();
        findViewById();
        handleEvent();
        setupRecycleView();
    }
    protected void getData(){
        position = getIntent().getIntExtra(StaticVariable.ALBUM_KEY,0);
        album = StaticVariable.albumsList.get(position);
    }
    protected void findViewById(){
        toolbar = (Toolbar)findViewById(R.id.album_detail_ac_toolbar);
        totalSongs = (TextView)findViewById(R.id.album_detail_ac_totalSongs);
        topToolbarImage = (ImageView)findViewById(R.id.album_detail_ac_image);
    }
    protected void handleEvent(){
        toolbar.setTitle(album.getAlbumName());
        topToolbarImage.setImageDrawable(Drawable.createFromPath(album.getImage()));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        totalSongs.setText("Total "+ album.getTotalSongs()+" Songs: ");
    }
    protected void setupRecycleView(){
        recyclerView= (RecyclerView)findViewById(R.id.album_detail_ac_recycleview);
        List<Songs> songs = GetSongInfoFromStorage.getSongImage(AlbumDetailActivity.this,StaticVariable.albumsList,album.getAlbumName());
        TabSongs.ContentAdapter adapter = new TabSongs.ContentAdapter(songs);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(AlbumDetailActivity.this));
    }
}
