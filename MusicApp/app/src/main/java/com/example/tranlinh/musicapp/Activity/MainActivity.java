package com.example.tranlinh.musicapp.Activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tranlinh.musicapp.Activity.TabsFragment.TabAlbums;
import com.example.tranlinh.musicapp.Activity.TabsFragment.TabArtists;
import com.example.tranlinh.musicapp.Activity.TabsFragment.TabSongs;
import com.example.tranlinh.musicapp.Object.Albums;
import com.example.tranlinh.musicapp.Object.Artists;
import com.example.tranlinh.musicapp.Object.Songs;
import com.example.tranlinh.musicapp.Object.StaticVariable;
import com.example.tranlinh.musicapp.R;
import com.example.tranlinh.musicapp.Support.GetSongInfoFromStorage.GetSongInfoFromStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    public View view;
    public TabLayout tabLayout;
    public Toolbar toolbar,btmToolBar;
    public ViewPager viewPager;
    public TextView songName,songDuration;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MainActivity.this, "Permision Read File is Granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Permision Read File is Denied", Toast.LENGTH_SHORT).show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public int initPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                //Permisson don't granted
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(MainActivity.this, "Permission isn't granted ", Toast.LENGTH_SHORT).show();
                }
                // Permisson don't granted and dont show dialog again.
                else {
                    Toast.makeText(MainActivity.this, "Permisson don't granted and dont show dialog again ", Toast.LENGTH_SHORT).show();
                }
                //Register permission
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return -1;
            } else{
                return 1;
            }
        }
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(initPermission() != -1) {
            StaticVariable.artistsList = new GetSongInfoFromStorage().getArtistInfo(MainActivity.this);
            StaticVariable.albumsList = new GetSongInfoFromStorage().getAlbumInfo(MainActivity.this);
            StaticVariable.songsList = new GetSongInfoFromStorage().getSongImage(MainActivity.this, StaticVariable.albumsList);
            StaticVariable.playList = StaticVariable.songsList;
            if (!StaticVariable.songsList.isEmpty()) {
                StaticVariable.currentSong = StaticVariable.playList.get(StaticVariable.currentPosition);
            }
        }
        findViewById();
        handleEvent();
    }
    protected void findViewById(){

        songName = (TextView)findViewById(R.id.mainac_btmToolBar_songName);
        songDuration = (TextView)findViewById(R.id.mainac_btmToolBar_duration);
        toolbar = (Toolbar)findViewById(R.id.mainac_toolBar);
        btmToolBar = (Toolbar)findViewById(R.id.mainac_btmToolBar);
        viewPager = (ViewPager)findViewById(R.id.mainac_viewPager);
        tabLayout = (TabLayout)findViewById(R.id.mainac_tabBar);
        view = (View) findViewById(R.id.activity_main);
    }
    protected void handleEvent(){
        if(!StaticVariable.songsList.isEmpty()){
            btmToolBar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this,PlaylistDetailActivity.class);
                    startActivity(intent);
                }
            });
            songName.setText(StaticVariable.songsList.get(StaticVariable.currentPosition).getName());

            int duration = Integer.parseInt(StaticVariable.songsList.get(StaticVariable.currentPosition).getDuration());
            String str = String.format("%s:%s", TimeUnit.MILLISECONDS.toMinutes(duration),TimeUnit.MILLISECONDS.toSeconds(duration)-
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
            songDuration.setText(str);

        }
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setSupportActionBar(toolbar);
    }
    private void setupViewPager(ViewPager viewPager){
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new TabSongs(),"Songs");
        adapter.addFragment(new TabArtists(),"Artists");
        adapter.addFragment(new TabAlbums(),"Albums");
        viewPager.setAdapter(adapter);
    }
    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("View Style");
            builder.setItems(R.array.view_style, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    view = (View) findViewById(R.id.activity_main);
                    Snackbar noti = Snackbar.make(view,"View style has changed",Snackbar.LENGTH_SHORT);
                    View sbView = noti.getView();
                    sbView.setBackgroundColor(Color.parseColor("#7A1EA1"));
                    sbView.setMinimumHeight(android.R.attr.actionBarSize);
                    noti.show();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        if(id == R.id.action_search && !StaticVariable.songsList.isEmpty()){
            Intent intent = new Intent(MainActivity.this,SearchActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
