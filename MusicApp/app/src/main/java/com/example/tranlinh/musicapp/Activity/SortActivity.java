package com.example.tranlinh.musicapp.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.example.tranlinh.musicapp.Activity.TabsFragment.TabSongs;
import com.example.tranlinh.musicapp.Object.StaticVariable;
import com.example.tranlinh.musicapp.R;
import com.example.tranlinh.musicapp.Support.MovingItemOnRecyclerView.SimpleItemTouchHelperCallback;

/**
 * Created by TranLinh on 12/15/2016.
 */

public class SortActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort);
        setSupportActionBar((Toolbar)findViewById(R.id.sortac_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupRecycleView();
    }
    protected void setupRecycleView(){
        recyclerView= (RecyclerView)findViewById(R.id.sortac_recycleview);
        TabSongs.ContentAdapter adapter = new TabSongs.ContentAdapter(StaticVariable.playList);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(SortActivity.this));

        ItemTouchHelper.Callback callback =
                new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
    }
}
