package com.example.tranlinh.musicapp.Object;

import android.content.Context;

import com.example.tranlinh.musicapp.Activity.MainActivity;
import com.example.tranlinh.musicapp.Support.GetSongInfoFromStorage.GetSongInfoFromStorage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TranLinh on 12/15/2016.
 */

public class StaticVariable {
    public static final String ALBUM_KEY = "ALBUM_POSITION";
    public static final String ARTIST_KEY = "ARTIST_POSITION";
    /**
     * pause = 0
     * play = 1
     */
    public static int isPlaying = 0;
    /**
     * not shuffle = 0
     * shuffle = 1
     */
    public static int isShuffling = 0;
    /**
    * repeat = 1
     * not repeat = 0
     */
    public static int isRepeating = 0;
    public static int currentPosition = 0;
    public static List<Songs> playList = new ArrayList<Songs>();
    public static List<Artists> artistsList = new ArrayList<Artists>();
    public static List<Albums> albumsList = new ArrayList<Albums>();
    public static List<Songs> songsList = new ArrayList<Songs>();
    public static Songs currentSong;
}
