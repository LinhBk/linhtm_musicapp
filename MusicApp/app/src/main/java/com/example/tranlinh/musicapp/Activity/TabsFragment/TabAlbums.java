package com.example.tranlinh.musicapp.Activity.TabsFragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tranlinh.musicapp.Activity.AlbumDetailActivity;
import com.example.tranlinh.musicapp.Object.Albums;
import com.example.tranlinh.musicapp.Object.StaticVariable;
import com.example.tranlinh.musicapp.R;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TranLinh on 12/14/2016.
 */

public class TabAlbums extends android.support.v4.app.Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView)inflater.inflate(R.layout.recycle_view,container,false);
        ContentAdapter adapter = new ContentAdapter(StaticVariable.albumsList);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        //recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));

        return recyclerView;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView albumName,albumArtist,albumTotalSong;
        public PorterShapeImageView albumImage;

        public ViewHolder(View itemView) {
            super(itemView);
        }
        public ViewHolder(LayoutInflater inflater,ViewGroup parent){

            super(inflater.inflate(R.layout.album_items,parent,false));
            albumImage = (PorterShapeImageView)itemView.findViewById(R.id.album_items_albumImage);
            albumName = (TextView)itemView.findViewById(R.id.album_items_albumName);
            albumArtist = (TextView)itemView.findViewById(R.id.album_items_albumArtist);
            albumTotalSong = (TextView)itemView.findViewById(R.id.album_items_albumTotalSongs);
        }
    }
    public static class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {
        // Set numbers of List in RecyclerView.
        private static final int LENGTH = 5;
        private List<Albums> albumsList = new ArrayList<Albums>();
        public ContentAdapter() {
        }
        public ContentAdapter(List<Albums> albumsList){
            this.albumsList = albumsList;
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            // no-op
            holder.albumName.setText(albumsList.get(position).getAlbumName());
            holder.albumArtist.setText("Artist: \t"+albumsList.get(position).getArtistName());
            holder.albumTotalSong.setText("Total Songs: \t"+String.valueOf(albumsList.get(position).getTotalSongs()));
            holder.albumImage.setImageDrawable(Drawable.createFromPath(albumsList.get(position).getImage()));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, AlbumDetailActivity.class);
                    intent.putExtra(StaticVariable.ALBUM_KEY,position);
                    context.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return albumsList.size();
        }
    }
}
