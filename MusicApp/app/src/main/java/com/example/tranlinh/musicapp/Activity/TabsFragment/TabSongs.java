package com.example.tranlinh.musicapp.Activity.TabsFragment;

import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tranlinh.musicapp.Activity.MainActivity;
import com.example.tranlinh.musicapp.Object.Songs;
import com.example.tranlinh.musicapp.Object.StaticVariable;
import com.example.tranlinh.musicapp.R;
import com.example.tranlinh.musicapp.Support.MovingItemOnRecyclerView.ItemTouchHelperAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by TranLinh on 12/14/2016.
 */

public class TabSongs extends android.support.v4.app.Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView)inflater.inflate(R.layout.recycle_view,container,false);
        ContentAdapter adapter = new ContentAdapter(StaticVariable.songsList);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        return recyclerView;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView songImage;
        public TextView songName;
        public TextView artistName;

        public ViewHolder(View itemView) {
            super(itemView);
        }
        public ViewHolder(LayoutInflater inflater,ViewGroup parent){
            super(inflater.inflate(R.layout.song_items,parent,false));
            songImage = (ImageView)itemView.findViewById(R.id.song_items_songImage);
            songName = (TextView)itemView.findViewById(R.id.song_items_songName);
            artistName = (TextView)itemView.findViewById(R.id.song_items_songArtist);
        }
    }
    public static class ContentAdapter extends RecyclerView.Adapter<ViewHolder> implements ItemTouchHelperAdapter{
        // Set numbers of List in RecyclerView.
        private List<Songs> songsList = new ArrayList<Songs>();

        public ContentAdapter(List<Songs> songsList){
            this.songsList = songsList;
        }


        public ContentAdapter() {
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            if(songsList.get(position).getImage()!=null) {
                holder.songImage.setImageDrawable(Drawable.createFromPath(songsList.get(position).getImage()));
            }
            holder.songName.setText(songsList.get(position).getName());
            holder.artistName.setText(songsList.get(position).getArtistName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                if(view.getContext() instanceof MainActivity){
                    MainActivity mainActivity = (MainActivity)view.getContext();
                    mainActivity.songName.setText(StaticVariable.songsList.get(position).getName());
                    StaticVariable.currentPosition = position;
                    int duration = Integer.parseInt(StaticVariable.songsList.get(StaticVariable.currentPosition).getDuration());
                    String str = String.format("%s:%s", TimeUnit.MILLISECONDS.toMinutes(duration),TimeUnit.MILLISECONDS.toSeconds(duration)-
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
                    mainActivity.songDuration.setText(str);
                }
                }
            });
        }

        @Override
        public int getItemCount() {
            return songsList.size();
        }

        @Override
        public void onItemMove(int fromPosition, int toPosition) {
            //Log.e("from: " + String.valueOf(fromPosition),"to: "+String.valueOf(toPosition));
            if(fromPosition < toPosition){
                for(int i = fromPosition; i< toPosition;i++){
                    Collections.swap(StaticVariable.playList,i,i+1);
                }
            } else{
                for(int i = fromPosition; i >toPosition;i--){
                    Collections.swap(StaticVariable.playList,i,i-1);
                }
            }
            notifyItemMoved(fromPosition,toPosition);
            for(int i = 0;i<StaticVariable.playList.size();i++){
                if(StaticVariable.playList.get(i).getName().equals(StaticVariable.currentSong.getName())){
                    StaticVariable.currentPosition = i;
                    break;
                }
            }
        }

        @Override
        public void onItemDismiss(int position) {
            //Log.e("position: ",String.valueOf(position));
            //StaticVariable.playList.remove(position);
            //notifyItemRemoved(position);
        }
    }
}
