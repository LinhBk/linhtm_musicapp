package com.example.tranlinh.musicapp.Support.MovingItemOnRecyclerView;

/**
 * Created by TranLinh on 12/16/2016.
 */

public interface ItemTouchHelperAdapter {
    void onItemMove(int fromPosition,int toPosition);
    void onItemDismiss(int position);
}
